module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("electron");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */
/***/ (function(module) {

module.exports = JSON.parse("{\"name\":\"seed-migration-tool\",\"version\":\"0.2.1\",\"author\":\"IOTA Foundation <contact@iota.org>\",\"description\":\"Seed migration tool for IOTA\",\"main\":\"dist/main.js\",\"scripts\":{\"postinstall\":\"electron-builder install-app-deps\",\"dev\":\"cross-env NODE_ENV=development webpack && concurrently \\\"npm run dev:app\\\" \\\"npm run dev:electron\\\"\",\"dev:app\":\"webpack-dev-server\",\"dev:electron\":\"cross-env NODE_ENV=development electron dist/main.js\",\"compile\":\"npm run build && electron-builder\",\"build\":\"rimraf dist/* && cross-env NODE_ENV=production webpack\"},\"engines\":{\"node\":\">= 10.19.0\"},\"build\":{\"files\":[\"dist/\",\"package.json\"],\"productName\":\"Seed Migration Tool\",\"artifactName\":\"seed-migration-tool-${version}.${ext}\",\"appId\":\"org.iota.seedmigration\",\"copyright\":\"IOTA Foundation\",\"afterSign\":\"./scripts/notarize.macos.js\",\"asar\":true,\"directories\":{\"output\":\"./out\",\"buildResources\":\"./assets\"},\"dmg\":{\"iconSize\":120,\"title\":\"${productName}\",\"sign\":false,\"contents\":[{\"x\":520,\"y\":225,\"type\":\"link\",\"path\":\"/Applications\"},{\"x\":170,\"y\":225,\"type\":\"file\"}]},\"nsis\":{\"deleteAppDataOnUninstall\":true},\"win\":{\"publisherName\":\"IOTA Stiftung\",\"target\":\"nsis\",\"timeStampServer\":\"http://timestamp.digicert.com\"},\"linux\":{\"target\":[\"AppImage\"],\"desktop\":{\"Name\":\"Seed Migration Tool\",\"Comment\":\"Seed migration tool for IOTA\",\"Categories\":\"Office;Network;Finance\"}},\"mac\":{\"category\":\"public.app-category.finance\",\"target\":[\"dmg\",\"zip\"],\"entitlements\":\"./entitlements.mac.plist\",\"entitlementsInherit\":\"./entitlements.mac.plist\",\"hardenedRuntime\":true,\"gatekeeperAssess\":false,\"asarUnpack\":[\"**/*.node\"]}},\"devDependencies\":{\"@types/argon2-browser\":\"1.12.0\",\"@types/kdbxweb\":\"1.2.0\",\"base64-loader\":\"1.0.0\",\"concurrently\":\"5.1.0\",\"copy-webpack-plugin\":\"5.1.1\",\"cross-env\":\"7.0.0\",\"css-loader\":\"3.4.2\",\"electron\":\"6.1.8\",\"electron-builder\":\"22.3.2\",\"electron-notarize\":\"0.2.1\",\"mini-css-extract-plugin\":\"0.9.0\",\"node-loader\":\"0.6.0\",\"rimraf\":\"3.0.2\",\"svelte\":\"3.18.2\",\"svelte-loader\":\"2.13.6\",\"ts-loader\":\"6.2.1\",\"typescript\":\"3.7.5\",\"webpack\":\"4.41.6\",\"webpack-cli\":\"3.3.11\",\"webpack-dev-server\":\"3.10.3\",\"webpack-livereload-plugin\":\"2.2.0\"},\"dependencies\":{\"argon2-browser\":\"1.13.0\",\"entangled-node\":\"iotaledger/entangled-node#8c5270e\",\"iota.lib.js\":\"0.5.2\",\"kdbxweb\":\"1.5.7\",\"micro-promisify\":\"0.1.1\",\"perfect-scrollbar\":\"^1.5.0\",\"zxcvbn\":\"^4.4.2\"}}");

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "electron"
var external_electron_ = __webpack_require__(0);

// EXTERNAL MODULE: ./package.json
var package_0 = __webpack_require__(6);

// CONCATENATED MODULE: ./src/native/menu.ts


const state = {
    enabled: true,
};
let language = {
    about: `Version ${package_0.version}`,
    errorLog: 'Error log',
    hide: 'Hide',
    quit: 'Quit',
    edit: 'Edit',
    undo: 'Undo',
    redo: 'Redo',
    cut: 'Cut',
    copy: 'Copy',
    paste: 'Paste',
    selectAll: 'Select All',
    documentation: 'Documentation',
};
/**
 * Create native menu tree and apply to the application window
 *
 * @param {App} app - Application object
 * @param {function} getWindow - Get Window instance helper
 *
 * @returns {void}
 */
const initMenu = (app, getWindow) => {
    let mainMenu;
    const navigate = (path) => {
        const window = getWindow();
        window.webContents.send('menu', path);
    };
    const createMenu = () => {
        const template = [
            {
                label: 'Seed Migration Tool',
                submenu: [
                    {
                        label: language.about,
                        enabled: false,
                    },
                    {
                        type: 'separator',
                    },
                    {
                        label: language.errorLog,
                        click: () => navigate('errorlog'),
                        enabled: state.enabled,
                    },
                    {
                        type: 'separator',
                    },
                ],
            },
        ];
        if (process.platform === 'darwin') {
            template[0].submenu = template[0].submenu.concat([
                {
                    label: `${language.hide} ${app.getName()}`,
                    role: 'hide',
                },
                {
                    type: 'separator',
                },
            ]);
        }
        template[0].submenu = template[0].submenu.concat([
            {
                label: language.quit,
                accelerator: 'Command+Q',
                click: function () {
                    app.quit();
                },
            },
        ]);
        template.push({
            label: language.edit,
            submenu: [
                { label: language.undo, accelerator: 'CmdOrCtrl+Z', selector: 'undo:' },
                { label: language.redo, accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:' },
                { type: 'separator' },
                { label: language.cut, accelerator: 'CmdOrCtrl+X', selector: 'cut:' },
                { label: language.copy, accelerator: 'CmdOrCtrl+C', selector: 'copy:' },
                { label: language.paste, accelerator: 'CmdOrCtrl+V', selector: 'paste:' },
                { label: language.selectAll, accelerator: 'CmdOrCtrl+A', selector: 'selectAll:' },
            ],
        });
        template.push({
            label: language.documentation,
            submenu: [
                {
                    label: `${language.documentation}`,
                    click: function () {
                        external_electron_["shell"].openExternal('https://docs.iota.org/docs/wallets/0.1/trinity/how-to-guides/protect-trinity-account');
                    },
                },
            ],
        });
        const applicationMenu = external_electron_["Menu"].buildFromTemplate(template);
        external_electron_["Menu"].setApplicationMenu(applicationMenu);
        return applicationMenu;
    };
    app.once('ready', () => {
        external_electron_["ipcMain"].on('menu.update', (e, settings) => {
            state[settings.attribute] = settings.value;
            mainMenu = createMenu();
        });
        external_electron_["ipcMain"].on('menu.enabled', (e, enabled) => {
            state.enabled = enabled;
            createMenu();
        });
        external_electron_["ipcMain"].on('menu.language', (e, data) => {
            language = data;
            mainMenu = createMenu();
        });
        external_electron_["ipcMain"].on('menu.popup', () => {
            const window = getWindow();
            mainMenu.popup(window);
        });
        mainMenu = createMenu();
    });
};

// EXTERNAL MODULE: external "path"
var external_path_ = __webpack_require__(1);
var external_path_default = /*#__PURE__*/__webpack_require__.n(external_path_);

// CONCATENATED MODULE: ./src/native/index.ts



const devMode = "production" === 'development';
/**
 * Terminate application if Node remote debugging detected
 */
const argv = process.argv.join();
if (argv.includes('inspect') || argv.includes('remote')) {
    external_electron_["app"].quit();
}
let mainWindow;
function createWindow() {
    const url = devMode ? 'http://localhost:3000' : `file://${external_path_default.a.join(__dirname, 'index.html')}`;
    const urlPreload = external_path_default.a.resolve(devMode ? __dirname : external_electron_["app"].getAppPath(), devMode ? '../' : './', 'dist/preload.js');
    mainWindow = new external_electron_["BrowserWindow"]({
        width: 1200,
        height: 740,
        minWidth: 720,
        minHeight: 720,
        frame: process.platform === 'linux',
        titleBarStyle: 'hidden',
        webPreferences: {
            nodeIntegration: false,
            disableBlinkFeatures: 'Auxclick',
            preload: urlPreload,
            webviewTag: false
        }
    });
    mainWindow.loadURL(url);
    if (devMode) {
        mainWindow.webContents.once('dom-ready', () => {
            mainWindow.webContents.openDevTools({ mode: 'detach' });
        });
    }
    mainWindow.on('closed', function () {
        mainWindow = null;
    });
    mainWindow.webContents.on('will-navigate', (e, targetURL) => {
        if (url.indexOf(targetURL) !== 0) {
            e.preventDefault();
            const whitelist = ['discord.iota.org', 'www.iota.org', 'status.iota.org', 'iota.org', 'thetangle.org', 'docs.iota.org', 'docs.google.com'];
            try {
                const parsedURL = new URL(targetURL);
                if (whitelist.indexOf(parsedURL.hostname) > -1) {
                    external_electron_["shell"].openExternal(targetURL);
                }
            }
            catch (error) {
                console.log(error);
            }
        }
    });
}
external_electron_["app"].on('ready', createWindow);
initMenu(external_electron_["app"], () => mainWindow);
external_electron_["app"].on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        external_electron_["app"].quit();
    }
});
external_electron_["app"].on('activate', function () {
    if (mainWindow === null) {
        createWindow();
    }
});


/***/ })
/******/ ]);