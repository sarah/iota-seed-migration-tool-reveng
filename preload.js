module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("electron");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var child_process__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var child_process__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(child_process__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1);
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var entangled_node__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4);
/* harmony import */ var entangled_node__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(entangled_node__WEBPACK_IMPORTED_MODULE_2__);
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};



let timeout = null;
/**
 * Spawn a child process and return result in async
 *
 * @param {string} payload - Payload to send to the child process
 *
 * @returns {Promise}
 */
const exec = (payload) => {
    return new Promise((resolve, reject) => {
        const child = Object(child_process__WEBPACK_IMPORTED_MODULE_0__["fork"])(path__WEBPACK_IMPORTED_MODULE_1___default.a.resolve(__dirname, 'entangled.js'));
        const { job } = JSON.parse(payload);
        child.on('message', (message) => {
            resolve(message);
            clearTimeout(timeout);
            child.kill();
        });
        timeout = setTimeout(() => {
            reject(`Timeout: Entangled job: ${job}`);
            child.kill();
        }, 30 * 1000);
        child.send(payload);
    });
};
/**
 * If module called as a child process, execute requested function and return response
 */
process.on('message', (data) => __awaiter(void 0, void 0, void 0, function* () {
    const payload = JSON.parse(data);
    if (payload.job === 'gen') {
        const address = yield entangled_node__WEBPACK_IMPORTED_MODULE_2___default.a.genAddressTrytesFunc(payload.seed, payload.index, payload.security);
        process.send(address);
    }
}));
const Entangled = {
    generateAddress: (seed, index, security) => __awaiter(void 0, void 0, void 0, function* () {
        return yield exec(JSON.stringify({ job: 'gen', seed, index, security }));
    }),
};
/* harmony default export */ __webpack_exports__["default"] = (Entangled);


/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("child_process");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("entangled-node");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 6 */,
/* 7 */,
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "electron"
var external_electron_ = __webpack_require__(0);

// EXTERNAL MODULE: external "fs"
var external_fs_ = __webpack_require__(5);
var external_fs_default = /*#__PURE__*/__webpack_require__.n(external_fs_);

// EXTERNAL MODULE: external "path"
var external_path_ = __webpack_require__(1);
var external_path_default = /*#__PURE__*/__webpack_require__.n(external_path_);

// EXTERNAL MODULE: ./src/native/entangled.ts
var entangled = __webpack_require__(2);

// CONCATENATED MODULE: ./src/native/helpers.ts
/**
 * Get current date in DD-MM-YYY-HH-MM format
 */
const getCurrentDate = () => {
    const today = new Date();
    const pad = (input) => input.toString().padStart(2, '0');
    return `${pad(today.getDate())}-${pad(today.getMonth() + 1)}-${today.getFullYear()}-${pad(today.getHours())}${pad(today.getMinutes())}`;
};

// CONCATENATED MODULE: ./src/native/preload.ts
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};





global['Electron'] = {
    getCurrentDate: getCurrentDate,
    getOS: () => {
        return process.platform;
    },
    _eventListeners: {},
    showMenu: () => {
        external_electron_["ipcRenderer"].send('menu.popup');
    },
    /**
     * Add native window wallet event listener
     *
     * @param {string} event - Target event name
     * @param {function} callback - Event trigger callback
     *
     * @returns {undefined}
     */
    onEvent: function (event, callback) {
        let listeners = this._eventListeners[event];
        if (!listeners) {
            listeners = this._eventListeners[event] = [];
            external_electron_["ipcRenderer"].on(event, (e, args) => {
                listeners.forEach((call) => {
                    call(args);
                });
            });
        }
        listeners.push(callback);
    },
    /**
     * Remove native window wallet event listener
     *
     * @param {string} event - Target event name
     * @param {function} callback - Event trigger callback
     *
     * @returns {undefined}
     */
    removeEvent: function (event, callback) {
        const listeners = this._eventListeners[event];
        listeners.forEach((call, index) => {
            if (call === callback) {
                listeners.splice(index, 1);
            }
        });
    },
    saveSeedVault: (buffer) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const targetPath = yield external_electron_["remote"].dialog.showSaveDialog(external_electron_["remote"].getCurrentWindow(), {
                title: 'Export SeedVault',
                defaultPath: `SeedVault-${getCurrentDate()}.kdbx`,
                buttonLabel: 'Export',
                filters: [{ name: 'SeedVault File', extensions: ['kdbx'] }]
            });
            if (targetPath.canceled) {
                throw Error('Export cancelled');
            }
            external_fs_default.a.writeFileSync(targetPath.filePath, Buffer.from(buffer));
            return true;
        }
        catch (error) {
            return false;
        }
    }),
    saveLogFile: (input, date) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const targetPath = yield external_electron_["remote"].dialog.showSaveDialog(external_electron_["remote"].getCurrentWindow(), {
                title: 'Export migration log',
                defaultPath: `MigrationLog-${date || getCurrentDate()}.json`,
                buttonLabel: 'Export',
                filters: [{ name: 'Migration log', extensions: ['json'] }]
            });
            if (targetPath.canceled) {
                throw Error('Export cancelled');
            }
            external_fs_default.a.writeFileSync(targetPath.filePath, input);
            return true;
        }
        catch (error) {
            return false;
        }
    }),
    savePaperWallet: () => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const targetPath = yield external_electron_["remote"].dialog.showSaveDialog(external_electron_["remote"].getCurrentWindow(), {
                title: 'Save IOTA seed template',
                defaultPath: `iota-seed-template.pdf`,
                buttonLabel: 'Save',
                filters: [{ name: 'PDF Document', extensions: ['pdf'] }]
            });
            if (targetPath.canceled) {
                throw Error('Export cancelled');
            }
            const devMode = "production" === 'development';
            const pdfPath = external_path_default.a.resolve(devMode ? __dirname : external_electron_["remote"].app.getAppPath(), devMode ? '../' : './', 'dist/wallet.pdf');
            external_fs_default.a.copyFileSync(pdfPath, targetPath.filePath);
            return true;
        }
        catch (error) {
            return false;
        }
    }),
    loadFile: () => __awaiter(void 0, void 0, void 0, function* () {
        const paths = yield external_electron_["remote"].dialog.showOpenDialog(external_electron_["remote"].getCurrentWindow(), {
            title: 'Import log file',
            buttonLabel: 'Import',
            filters: [{ name: 'Seed migration log file', extensions: ['json'] }]
        });
        if (paths.canceled || paths.filePaths.length !== 1) {
            return null;
        }
        const content = external_fs_default.a.readFileSync(paths.filePaths[0]);
        return content.toString();
    }),
    generateAddress: (seed, index, security, total) => __awaiter(void 0, void 0, void 0, function* () {
        if (!total || total === 1) {
            return entangled["default"].generateAddress(seed, index, security);
        }
        const addresses = [];
        for (let i = 0; i < total; i++) {
            const address = yield entangled["default"].generateAddress(seed, index + i, security);
            addresses.push(address);
        }
        return addresses;
    }),
    minimize: () => {
        external_electron_["remote"].getCurrentWindow().minimize();
    },
    maximize: () => {
        const window = external_electron_["remote"].getCurrentWindow();
        if (window.isMaximized()) {
            window.unmaximize();
        }
        else {
            window.maximize();
        }
    },
    close: () => {
        external_electron_["remote"].getCurrentWindow().close();
    }
};


/***/ })
/******/ ]);